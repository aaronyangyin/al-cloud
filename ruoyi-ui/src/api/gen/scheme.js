import request from '@/utils/request'

// 查询代码生成模板组管理列表
export function listScheme(query) {
  return request({
    url: '/code/scheme/list',
    method: 'get',
    params: query
  })
}// 查询代码生成模板组管理列表
export function getAll(query) {
  return request({
    url: '/code/scheme/getAll',
    method: 'get',
    params: query
  })
}

// 查询代码生成模板组管理详细
export function getScheme(id) {
  return request({
    url: '/code/scheme/' + id,
    method: 'get'
  })
}

// 新增代码生成模板组管理
export function addScheme(data) {
  return request({
    url: '/code/scheme',
    method: 'post',
    data: data
  })
}

// 修改代码生成模板组管理
export function updateScheme(data) {
  return request({
    url: '/code/scheme',
    method: 'put',
    data: data
  })
}

// 删除代码生成模板组管理
export function delScheme(id) {
  return request({
    url: '/code/scheme/' + id,
    method: 'delete'
  })
}
