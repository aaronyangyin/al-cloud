package com.ruoyi.gen.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ruoyi.gen.domain.TestCase;

import java.util.List;

/**
 * 测试用例Service接口
 *
 * @author ruoyi
 * @date 2020-12-24
 */
public interface ITestCaseService extends IService<TestCase> {


    /**
     * 查询测试用例列表
     *
     * @param testCase 测试用例
     * @return 测试用例集合
     */
    List<TestCase> selectTestCaseList(TestCase testCase);


}
