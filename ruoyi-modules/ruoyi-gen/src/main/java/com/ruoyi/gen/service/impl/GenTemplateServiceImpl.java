package com.ruoyi.gen.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruoyi.gen.domain.GenTemplate;
import com.ruoyi.gen.domain.entity.GenTemplateEntity;
import com.ruoyi.gen.mapper.GenTemplateMapper;
import com.ruoyi.gen.service.IGenTemplateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 代码生成模板管理1Service业务层处理
 *
 * @author ruoyi
 * @date 2020-12-24
 */
@Service
public class GenTemplateServiceImpl extends ServiceImpl<GenTemplateMapper, GenTemplate> implements IGenTemplateService {
    @Autowired
    private GenTemplateMapper genTemplateMapper;


    /**
     * 查询代码生成模板管理1列表
     *
     * @param genTemplate 代码生成模板管理1
     * @return 代码生成模板管理1
     */
    @Override
    public List<GenTemplateEntity> selectGenTemplateList(GenTemplateEntity genTemplate) {
        return genTemplateMapper.selectGenTemplateList(genTemplate);
    }

    @Override

    public List<GenTemplate> selectGenTemplateListBySid(Long schemeId) {
        GenTemplate genTemplate = new GenTemplate();
        genTemplate.setSchemeId(schemeId);
        QueryWrapper<GenTemplate> queryWrapper = new QueryWrapper<GenTemplate>(genTemplate);
        return genTemplateMapper.selectList(queryWrapper);
    }


}
