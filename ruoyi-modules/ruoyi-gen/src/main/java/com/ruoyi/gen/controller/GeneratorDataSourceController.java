package com.ruoyi.gen.controller;

import com.common.zrd.json.CommonJsonResult;
import com.ruoyi.common.core.utils.poi.ExcelUtil;
import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.core.web.page.TableDataInfo;
import com.ruoyi.common.log.annotation.Log;
import com.ruoyi.common.log.enums.BusinessType;
import com.ruoyi.common.security.annotation.PreAuthorize;
import com.ruoyi.gen.domain.GeneratorDataSource;
import com.ruoyi.gen.service.IGeneratorDataSourceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

/**
 * 数据源Controller
 *
 * @author ruoyi
 * @date 2020-12-24
 */
@RestController
@RequestMapping("/source" )
public class GeneratorDataSourceController extends BaseController {
    @Autowired
    private IGeneratorDataSourceService generatorDataSourceService;

    /**
     * 查询数据源列表
     */
    @PreAuthorize(hasPermi = "gen:source:list" )
    @GetMapping("/list" )
    public TableDataInfo list(GeneratorDataSource generatorDataSource) {
        startPage();
        List<GeneratorDataSource> list = generatorDataSourceService.selectGeneratorDataSourceList(generatorDataSource);
        return getDataTable(list);
    }


    /**
     * 查询数据源列表
     */
    @PreAuthorize(hasPermi = "gen:source:list" )
    @GetMapping("/getAll" )
    public CommonJsonResult getAll(GeneratorDataSource generatorDataSource) {
        List<GeneratorDataSource> list = generatorDataSourceService.selectGeneratorDataSourceList(generatorDataSource);
        return CommonJsonResult.of(list);
    }
    /**
     * 导出数据源列表
     */
    @PreAuthorize(hasPermi = "gen:source:export" )
    @Log(title = "数据源" , businessType = BusinessType.EXPORT)
    @PostMapping("/export" )
    public void export(HttpServletResponse response, GeneratorDataSource generatorDataSource) throws IOException {
        List<GeneratorDataSource> list = generatorDataSourceService.selectGeneratorDataSourceList(generatorDataSource);
        ExcelUtil<GeneratorDataSource> util = new ExcelUtil<>(GeneratorDataSource.class);
        util.exportExcel(response, list, "source" );
    }

    /**
     * 获取数据源详细信息
     */
    @PreAuthorize(hasPermi = "gen:source:query" )
    @GetMapping(value = "/{id}" )
    public AjaxResult getInfo(@PathVariable("id" ) Long id) {
        return AjaxResult.success(generatorDataSourceService.getById(id));
    }

    /**
     * 新增数据源
     */
    @PreAuthorize(hasPermi = "gen:source:add" )
    @Log(title = "数据源" , businessType = BusinessType.INSERT)
    @PostMapping
    public CommonJsonResult add(@RequestBody GeneratorDataSource generatorDataSource) {
        generatorDataSourceService.save(generatorDataSource);
        return CommonJsonResult.of(generatorDataSource);
    }

    /**
     * 修改数据源
     */
    @PreAuthorize(hasPermi = "gen:source:edit" )
    @Log(title = "数据源" , businessType = BusinessType.UPDATE)
    @PutMapping
    public CommonJsonResult edit(@RequestBody GeneratorDataSource generatorDataSource) {
        generatorDataSourceService.updateById(generatorDataSource);
        return CommonJsonResult.of(generatorDataSource);
    }

    /**
     * 删除数据源
     */
    @PreAuthorize(hasPermi = "gen:source:remove" )
    @Log(title = "数据源" , businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}" )
    public CommonJsonResult remove(@PathVariable Long[] ids) {

        generatorDataSourceService.removeByIds(Arrays.asList(ids));
        return CommonJsonResult.empty();
    }
}
